#include "chip8emu.hpp"

chip8emu::chip8emu(): 
memory{0xF0,0x90,0x90,0x90,0xF0,
	0x20,0x60,0x20,0x20,0x70,
	0xF0,0x10,0xF0,0x80,0xF0,
	0xF0,0x10,0xF0,0x10,0xF0,
	0x90,0x90,0xF0,0x10,0x10,
	0xF0,0x80,0xF0,0x10,0xF0,
	0xF0,0x80,0xF0,0x90,0xF0,
	0xF0,0x10,0x20,0x40,0x40,
	0xF0,0x90,0xF0,0x90,0xF0,
	0xF0,0x90,0xF0,0x10,0xF0,
	0xF0,0x90,0xF0,0x90,0x90,
	0xE0,0x90,0xE0,0x90,0xE0,
	0xF0,0x80,0x80,0x80,0xF0,
	0xE0,0x90,0x90,0x90,0xE0,
	0xF0,0x80,0xF0,0x80,0xF0,
	0xF0,0x80,0xF0,0x80,0x80}, 
V(), DT(), ST(), gmemory(), I(), PC(0x200),
keymap{{1,sf::Keyboard::Num1},{2,sf::Keyboard::Num2},{3,sf::Keyboard::Num3},{0xC,sf::Keyboard::Num4},
	{4,sf::Keyboard::Q},{5,sf::Keyboard::W},{6,sf::Keyboard::E},{0xD,sf::Keyboard::R},
	{7,sf::Keyboard::A},{8,sf::Keyboard::S},{9,sf::Keyboard::D},{0xE,sf::Keyboard::F}, 
	{0xA,sf::Keyboard::Z},{0,sf::Keyboard::X},{0xB,sf::Keyboard::C},{0xF,sf::Keyboard::V}},
window(sf::VideoMode(640,320),"chip8 emulator by M.P") {
	for(int i = 0; i < 64; ++i) {
		for(int j = 0; j < 32; ++j) {
			rect[i][j].setPosition(i*10,j*10);
			rect[i][j].setSize(sf::Vector2f(10,10));
		}
	}
} 

void chip8emu::cycle() {
	uint16_t instr = memory[PC++];
	instr = instr<<8 | memory[PC++];
	
	uint16_t nnn = instr & 0x0FFF; 	
	uint8_t kk = instr & 0x00FF;
	uint8_t x = (instr & 0x0F00) >> 8;
	uint8_t y = (instr & 0x00F0) >> 4;

	switch(instr & 0xF000) {
		case 0x1000: 
			PC = nnn;
			break;
		case 0x2000:
			cpustack.push(PC);
			PC = nnn;
			break;
		case 0x3000:
			if(V[x] == kk) PC += 2;
			break;
		case 0x4000:
			if(V[x] != kk) PC += 2;
			break;
		case 0x6000:
			V[x] = kk;
			break;
		case 0x7000:
			V[x] += kk;
			break;
		case 0xA000:
			I = nnn;
			break;
		case 0xB000:
			PC = nnn + V[0];
			break;
		case 0xC000: 
			V[x] = std::uniform_int_distribution<uint8_t>(0,255)(rnddev) & kk;
			break;
		case 0xD000:
			cy = V[y];
			for(int i = 0; i < (kk&0x0F); ++i, ++cy) {
				cx = V[x];
				for(int j = 0; j < 8; ++j, ++cx) {
					V[0xF] = ((gmemory[cx][cy] | ((memory[I+i] >> (7-j)) & 1)) != (gmemory[cx][cy] ^ ((memory[I+i] >> (7-j)) & 1))); 
					gmemory[cx][cy] ^= ((memory[I+i] >> (7-j)) & 1);
				}
			}
			break;
	}

	switch(instr & 0xF00F) {
		case 0x5000:
			if(V[x] == V[y]) PC +=2;
			break;
		case 0x8000:
			V[x] = V[y];
			break;
		case 0x8001:
			V[x] |= V[y];
			break;
		case 0x8002:
			V[x] &= V[y];
			break;
		case 0x8003:
			V[x] ^= V[y];
			break;
		case 0x8004:
			V[0xF] = (uint16_t(V[x]) + V[y] > 255); 
			V[x] += V[y]; 
			break;
		case 0x8005: 
			V[0xF] = (V[x] > V[y]); 
			V[x] -= V[y];
			break;
		case 0x8006:
			V[0xF] = (V[x] & 1); 
			V[x] >>= 1;
			break;
		case 0x8007:
			V[0xF] = (V[y] > V[x]); 
			V[x] = V[y] - V[x];
			break;
		case 0x800E:
			V[0xF] = (V[x]>>7 & 1);
			V[x] <<= 1;
 			break;
		case 0x9000:
			if(V[x] != V[y]) PC +=2;
			break;
	}

	switch(instr & 0xF0FF) {
		case 0x00E0:
			std::fill(&gmemory[0][0], &gmemory[0][0]+sizeof(gmemory), 0);
			break;
		case 0x00EE:
			PC = cpustack.top();
			cpustack.pop();
			break;
		case 0xE09E:
			if(sf::Keyboard::isKeyPressed(keymap[V[x]])) PC+=2;
			break;
		case 0xE0A1:
			if(!sf::Keyboard::isKeyPressed(keymap[V[x]])) PC+=2;
			break;
		case 0xF007:
			V[x] = DT;	
			break;
		case 0xF00A:
			for(int i = 0; i < 0xF; ++i) {
				if(sf::Keyboard::isKeyPressed(keymap[i])) {
					V[x] = i; 
					PC+=2; 
					break;
				}
			}
			PC-=2;
			break;
		case 0xF015:
			DT = V[x]; 
			updateTimers(false);  
			break;
		case 0xF018:
			ST = V[x];
			updateTimers(false);
			break;
		case 0xF01E:
			I += V[x];	
			break;
		case 0xF029:
			I = V[x]*5;
			break;
		case 0xF033:
			memory[I] = (V[x] / 100); 		
			memory[I+1] = (V[x] / 10) % 10; 
			memory[I+2] = V[x] % 10; 		
			break;
		case 0xF055:
			for(int i = 0; i <= x; ++i) memory[I+i] = V[i];
			break;
		case 0xF065:
			for(int i = 0; i <= x; ++i) V[i] = memory[I+i];
			break;	
	}
	
	updateTimers(true);

}

void chip8emu::draw() {
	window.clear();
	for(int i = 0; i < 64; ++i) for(int j = 0; j < 32; ++j) if(gmemory[i][j]) window.draw(rect[i][j]);
	window.display();
}

void chip8emu::updateTimers(bool checkFreq) {
	switch (checkFreq) {
		case true:
			if(std::chrono::steady_clock::now() - tp < std::chrono::milliseconds{16}) break;		
		case false:
			if(ST > 0) --ST;
			if(DT > 0) --DT; 
			tp = std::chrono::steady_clock::now();		
			break;
	}	
}

void chip8emu::loadFile(char * filename, uint16_t mempos) {
	std::ifstream rom;
	rom.open(filename, std::ios::binary);
	while(rom.good()) memory[mempos++] = rom.get();
}
