#include "chip8emu.hpp"


int main(int argc, char ** argv) {

    	chip8emu emulator;
	emulator.loadFile("TICTAC",0x200);
	
	auto tp = std::chrono::steady_clock::now();
	while (emulator.window.isOpen()) {
		if(std::chrono::steady_clock::now() - tp > std::chrono::microseconds(1851)) emulator.cycle(), tp = std::chrono::steady_clock::now();	
		emulator.draw();
	} 

return 0; 
}
