#include <fstream>
#include <stack>
#include <random>
#include <algorithm>
#include <chrono>
#include <SFML/Graphics.hpp>
#include <map>
#include <iostream>

class chip8emu {
	private:
		uint8_t memory[4096], V[16], DT, ST, gmemory[64][32], cx:6, cy:5;
		uint16_t I, PC;
		std::stack<uint16_t> cpustack; 
		std::mt19937 rnddev{};
		std::chrono::steady_clock::time_point tp; 
		std::map<uint8_t,sf::Keyboard::Key> keymap;
		void updateTimers(bool checkFreq);
	
	public:
		sf::RenderWindow window;
		sf::RectangleShape rect[64][32];
		chip8emu();
		void cycle();
		void draw();
		void loadFile(char *, uint16_t);			
};


