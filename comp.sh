#!/bin/bash

SPATH="`dirname \"$0\"`"
SPATH="`( cd \"$SPATH\" && pwd )`"
cd $SPATH

g++ -o bin/chip8emu src/main.cpp src/chip8emu.cpp -lsfml-graphics -lsfml-window -lsfml-system -g -Wall -std=c++14 
bin/chip8emu
