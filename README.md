# chip8 emulator

A chip8 emulator written in C++ and SFML, purely for educational purposes. While developing i realised that there are diffrent chip8 specifications and it sadly means that you can never know if a loaded ROM will work or not so I haven't even implemented a convinient way to load them - when You launch the emulator it automatially loads a file called TICTAC.

## Prerequisites

The emulator is desingned to be compiled and run on unix systems. To be able to do that You will need GCC and SFML library (and preferably a GNU/Linux distribution). The comp.sh bash script compiles and launches the emulator. If You just want to launch it then make sure that your working directory is where the script is, otherwise the program won't see the ROM file.

## Keybindings

chip8 had a limited and explicitly defined set of supported keys so I've had to map them (the first table represents original chip8 keys and the second one the mappings - corresponding cells)

|||||
|:-:|:-:|:-:|:-:|
| 1 | 2 | 3 | C |
| 4 | 5 | 6 | D |
| 7 | 8 | 9 | E |
| A | 0 | B | F |

|||||
|:-:|:-:|:-:|:-:|
| 1 | 2 | 3 | 4 |
| Q | W | E | R |
| A | S | D | F |
| Z | X | C | V |

